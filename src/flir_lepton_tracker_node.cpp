/*
 * trackerMain.cpp
 *
 *  Created on: Oct 6, 2015
 *      Author: taylordean
 */

#include <iostream>
#include <cmath>
#include <chrono>
#include <thread>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>
#include <std_msgs/Float32MultiArray.h>
#include <cv_bridge/cv_bridge.h>
#include "flir_lepton_tracker/flir_lepton_tracker_node.hpp"
#include "flir_lepton_tracker/OBJECT.hpp"
#include "flir_lepton_tracker/OBJECT_CONTAINER.hpp"

using namespace cv;
using namespace std;

// ENVIRONMENT VARIABLES
int SHOW_TRACKING           = atoi(getenv("SHOW_TRACKING"));
int DEBUG_LEVEL             = atoi(getenv("DEBUG_LEVEL"));
int SPOOF_DEPTH             = atoi(getenv("SPOOF_DEPTH"));
int SPOOF_FLIR              = atoi(getenv("SPOOF_FLIR"));
int PERFORM_CALIBRATION     = atoi(getenv("PERFORM_CALIBRATION"));
int MAX_OBJECTS             = atoi(getenv("MAX_OBJECTS"));
float MIN_OBJECT_AREA_PX    = atof(getenv("MIN_OBJECT_AREA_PX"));
float MAX_OBJECT_AREA_PX    = atof(getenv("MAX_OBJECT_AREA_PX"));
float MAX_OBJECT_AREA_M     = atof(getenv("MAX_OBJECT_AREA_M"));
int DEPTH_DOWNSELECT_ON     = atoi(getenv("DEPTH_DOWNSELECT_ON"));
int FLIR_ROWS               = atoi(getenv("FLIR_ROWS"));
int FLIR_COLS               = atoi(getenv("FLIR_COLS"));
int DEPTH_ROWS              = atoi(getenv("DEPTH_ROWS"));
int DEPTH_COLS              = atoi(getenv("DEPTH_COLS"));
float FLIR_HFOV             = atof(getenv("FLIR_HFOV"))*PI/180;
float FLIR_VFOV             = atof(getenv("FLIR_VFOV"))*PI/180;
float DEPTH_HFOV            = atof(getenv("DEPTH_HFOV"))*PI/180;
float DEPTH_VFOV            = atof(getenv("DEPTH_VFOV"))*PI/180;
float RAW_TARGET_INTENSITY  = atof(getenv("RAW_TARGET_INTENSITY"));
float INTENSITY_BOUND_WIDTH = atof(getenv("INTENSITY_BOUND_WIDTH"));
float DEPTH_EL_OFFSET       = atof(getenv("DEPTH_EL_OFFSET"))*PI/180;
float DEPTH_AZ_OFFSET       = atof(getenv("DEPTH_AZ_OFFSET"))*PI/180;

// OTHER GLOBAL VARIABLES
auto START_TIME               = chrono::high_resolution_clock::now();
bool CALIBRATION_COMPLETE     = false;
bool READING_DEPTH_IMAGE      = false;
bool READING_IR_IMAGE         = false;
bool SETTING_DEPTH_IMAGE      = false;
bool SETTING_IR_IMAGE         = false;
bool SUBSCRIBERS_STARTED      = false;
bool DEPTH_IMAGE_UPDATED      = false;
bool IR_IMAGE_UPDATED         = false;
float INTENSITY_UPPER_BOUND   = RAW_TARGET_INTENSITY + INTENSITY_BOUND_WIDTH;
float INTENSITY_LOWER_BOUND   = RAW_TARGET_INTENSITY - INTENSITY_BOUND_WIDTH;
float FLIR_H_LENGTH           = FLIR_COLS/tan(FLIR_HFOV/2)/2;
float FLIR_V_LENGTH           = FLIR_ROWS/tan(FLIR_VFOV/2)/2;
float DEPTH_H_LENGTH          = DEPTH_COLS/tan(DEPTH_HFOV/2)/2;
float DEPTH_V_LENGTH          = DEPTH_ROWS/tan(DEPTH_VFOV/2)/2;

Mat DEPTH_IMAGE;
Mat IR_IMAGE;

bool isTargetGood(const OBJECT target)
{
    // CHECK THAT THE AREA OF THE TARGET MAKES SENSE AT THE GIVEN RANGE
    float totalFovArea = 4*target.range*target.range*sin(FLIR_HFOV/2)*sin(FLIR_VFOV/2);
    float maxArea = MAX_OBJECT_AREA_M*FLIR_COLS*FLIR_ROWS/totalFovArea;
  
    if (DEBUG_LEVEL > 1) {
        fprintf(stdout, "totalFovArea = %fm\tmaxArea = %fpx\ttarget area = %fpx\ttarget range = %f\n", totalFovArea, maxArea, target.area, target.range);
    }

    bool isGood = true;
    if ((target.area > maxArea) || (totalFovArea == 0) || isnan(totalFovArea) || isinf(totalFovArea)) {
        if (DEPTH_DOWNSELECT_ON) {
            isGood = false;
        }
    
        if (DEBUG_LEVEL > 1) {
            fprintf(stdout, "\tDELETE");
        }
    }
    
    if (DEBUG_LEVEL > 1) {
        fprintf(stdout, "\n");
        fflush(stdout);
    }

    if ((DEBUG_LEVEL > 2) && isGood) {
        fprintf(stdout, "az = %f\tel = %f\trow = %d\tcol = %d\n", target.az, target.el, target.depthLoc.x, target.depthLoc.y);
        fflush(stdout);
    }

    return isGood;
}

void calculateObjectParameters(OBJECT& target, const Mat imgDepth, const Mat imgOriginal)
{
    float xloc = target.centroid.x + 1;
    float yloc = target.centroid.y + 1;

    double az = atan2(-(imgOriginal.cols/2 - xloc), FLIR_H_LENGTH);
    double el = atan2(-(yloc - imgOriginal.rows/2), FLIR_V_LENGTH);

    float depthx = DEPTH_H_LENGTH*tan(az - DEPTH_AZ_OFFSET) + imgDepth.cols/2;
    float depthy = imgDepth.rows/2 - DEPTH_V_LENGTH*tan(el - DEPTH_EL_OFFSET);

    double dist = 0.0;
    if ((depthy > 0) && (depthy < imgDepth.cols) && (depthx > 0) && (depthx < imgDepth.rows)) {
        dist = imgDepth.at<float>(depthx, depthy);
    }

    target.az = az;
    target.el = el;
    target.range = dist;
    target.xpos = -dist*cos(el)*sin(az);
    target.ypos = dist*cos(el)*cos(az);
    target.zpos = dist*sin(el);
    target.depthLoc.x = depthx;
    target.depthLoc.y = depthy;
    target.flirLoc.x = yloc;
    target.flirLoc.y = xloc;
}

void getDepthImage(const sensor_msgs::ImageConstPtr& msg)
{
    if (!READING_DEPTH_IMAGE) {
        SETTING_DEPTH_IMAGE = true;
        cv_bridge::CvImagePtr cv_ptr;
        try {
            cv_bridge::CvImagePtr cvPtr = cv_bridge::toCvCopy(msg, "");
            DEPTH_IMAGE = cvPtr->image.clone();
            DEPTH_IMAGE_UPDATED = true;
        } catch (cv_bridge::Exception &e) {
            ROS_ERROR("Could not convert depth image from '%s' to 'mono16'.", msg->encoding.c_str());
        }
        SETTING_DEPTH_IMAGE = false;
    }
}

void getFlirImage(const sensor_msgs::ImageConstPtr& msg)
{
    if (!READING_IR_IMAGE) {
        SETTING_IR_IMAGE = true;
        cv_bridge::CvImagePtr cv_ptr;
        try {
            cv_bridge::CvImagePtr cvPtr = cv_bridge::toCvCopy(msg, "mono16");
            IR_IMAGE = cvPtr->image.clone();
            IR_IMAGE_UPDATED = true;
        } catch (cv_bridge::Exception &e) {
            ROS_ERROR("Could not convert flir image from '%s' to 'mono16'.", msg->encoding.c_str());
        }
        SETTING_IR_IMAGE = false;
    }
    SUBSCRIBERS_STARTED = true;
}

void applyGain(const Mat imgIn, Mat& imgOut)
{
    float minValue = 65536;
    float maxValue = 0;

    for (int rowIdx = 0; rowIdx < imgIn.rows; rowIdx++) {
        for (int colIdx = 0; colIdx < imgIn.cols; colIdx++) {
            float val = imgIn.at<uint16_t>(rowIdx, colIdx);
            if (val > maxValue) maxValue = val;
            if (val < minValue) minValue = val;
        }
    }
    
    float diff = maxValue - minValue + 1;
    for (int rowIdx = 0; rowIdx < imgIn.rows; rowIdx++) {
        for (int colIdx = 0; colIdx < imgIn.cols; colIdx++) {
            float val = imgIn.at<uint16_t>(rowIdx, colIdx);
            uint16_t newVal = (65536.0*(val - minValue)/diff);
            imgOut.at<uint16_t>(rowIdx, colIdx) = newVal;
        }
    }
}

void morphImage(const Mat imgIn, Mat& imgOut, const int iLow, const int iHigh)
{
    inRange(imgIn, iLow, iHigh, imgOut);

    //morphological opening (removes small objects from the foreground)
    erode(imgOut, imgOut, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
    dilate(imgOut, imgOut, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));

    //morphological closing (removes small holes from the foreground)
    dilate(imgOut, imgOut, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
    erode(imgOut, imgOut, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
}

void getTargets(OBJECT_CONTAINER& objects, Mat imgContour, const Mat imgDepth, const Mat imgOriginal)
{
    vector< vector < Point > > contours;
    vector< vector < Point > > finalContours;
    vector< double > areas;
    time_t gmt = time(0);
    findContours(imgContour, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

    for (unsigned int contourIdx=0; contourIdx<contours.size(); contourIdx++) {
        double area = contourArea(contours[contourIdx], false);
        if ((area > MIN_OBJECT_AREA_PX) && (area < MAX_OBJECT_AREA_PX)) {
            areas.push_back(area);
            finalContours.push_back(contours[contourIdx]);
        }
    }

    for (int retIdx=0; retIdx<MAX_OBJECTS; retIdx++) {
        if (finalContours.empty()) {
            break;
        } else {
            int maxIdx = 0;
            for (unsigned int contourIdx=0; contourIdx<finalContours.size(); contourIdx++) {
                if (areas[maxIdx] < areas[contourIdx]) {
                    maxIdx = contourIdx;
                }
            }

            Point centroid;
            findCentroid(finalContours[maxIdx], centroid);
            Rect brect = boundingRect(Mat(finalContours[maxIdx]).reshape(2));

            OBJECT newObject(objects.size(), -1, gmt, areas[maxIdx], centroid, brect);
            calculateObjectParameters(newObject, imgDepth, imgOriginal);

            if (isTargetGood(newObject)) {
                objects.append(newObject);
            }

            finalContours.erase(finalContours.begin() + maxIdx);
            areas.erase(areas.begin() + maxIdx);
        }
    }
}

void findCentroid(const vector< Point > points, Point& centroid)
{
    float sumx = 0;
    float sumy = 0;
    for (unsigned int i=0; i<points.size(); i++) {
        sumx = sumx + points[i].x;
        sumy = sumy + points[i].y;
    }
    int posX = sumx / points.size();
    int posY = sumy / points.size();

    centroid = Point(posX, posY);
}

void processImages(const Mat imgDepth, const Mat imgOriginal, Mat& imgThresholded, OBJECT_CONTAINER& GLOBAL_RETURNS)
{
    morphImage(imgOriginal, imgThresholded, INTENSITY_LOWER_BOUND, INTENSITY_UPPER_BOUND);
    getTargets(GLOBAL_RETURNS, imgThresholded.clone(), imgDepth, imgOriginal);
}

void doTracking(image_transport::Publisher irTrackedPub, image_transport::Publisher irMaskedPub, image_transport::Publisher depthTrackedPub, ros::Publisher targetPosPub)
{
    while (true) {
        if (SPOOF_DEPTH == 1) {
            DEPTH_IMAGE_UPDATED = true;
        }
        if (SPOOF_FLIR == 1) {
            SUBSCRIBERS_STARTED = true;
            IR_IMAGE_UPDATED = true;
        }

        if (!SUBSCRIBERS_STARTED || !DEPTH_IMAGE_UPDATED || !IR_IMAGE_UPDATED) {
            if (DEBUG_LEVEL >= 9) {
                fprintf(stdout, "SUBSCRIBERS_STARTED=%u\tDEPTH_IMAGE_UPDATED=%u\tIR_IMAGE_UPDATED=%u\n", SUBSCRIBERS_STARTED, DEPTH_IMAGE_UPDATED, IR_IMAGE_UPDATED);
                fflush(stdout);
            }
            continue;
        }

        // ALLOCATE OBJECT_CONTAINER
        OBJECT_CONTAINER GLOBAL_RETURNS;

        // READ IN THERMAL IMAGE AND DEPTH IMAGE
        Mat imgDepth;
        Mat imgOriginal;

        if (!SETTING_DEPTH_IMAGE) {
            READING_DEPTH_IMAGE = true;
            if (SPOOF_DEPTH == 0) {
                imgDepth = DEPTH_IMAGE.clone();
            } else {
                imgDepth = Mat::ones(DEPTH_ROWS, DEPTH_COLS, CV_16UC1);
                for (int rowIdx = 0; rowIdx < DEPTH_ROWS; rowIdx++) {
                    for (int colIdx = 0; colIdx < DEPTH_COLS; colIdx++) {
                        imgDepth.at<uint16_t>(rowIdx, colIdx) = rand()*65535/RAND_MAX;
                    }
                }
            }
            READING_DEPTH_IMAGE = false;
            DEPTH_IMAGE_UPDATED = false;
        } else {
            continue;
        }

        if (!SETTING_IR_IMAGE) {
            READING_IR_IMAGE = true;
            if (SPOOF_FLIR == 0) {
                imgOriginal = IR_IMAGE.clone();
            } else {
                imgOriginal = Mat::ones(FLIR_ROWS, FLIR_COLS, CV_16UC1);
                for (int rowIdx = 0; rowIdx < FLIR_ROWS; rowIdx++) {
                    for (int colIdx = 0; colIdx < FLIR_COLS; colIdx++) {
                        imgDepth.at<uint16_t>(rowIdx, colIdx) = rand()*65535/RAND_MAX;
                    }
                }
            }
            READING_IR_IMAGE = false;
            IR_IMAGE_UPDATED = false;
        } else {
            continue;
        }

        // PERFORM CALIBRATION
        if (!CALIBRATION_COMPLETE && (PERFORM_CALIBRATION == 1)) {
            RAW_TARGET_INTENSITY = mean(imgOriginal).val[0];
            CALIBRATION_COMPLETE = true;
            
            INTENSITY_LOWER_BOUND = RAW_TARGET_INTENSITY - INTENSITY_BOUND_WIDTH;
            INTENSITY_UPPER_BOUND = RAW_TARGET_INTENSITY + INTENSITY_BOUND_WIDTH;
        }

        if (DEBUG_LEVEL > 1) {
            fprintf(stdout, "Averaged unscaled target intensity = %f\n", RAW_TARGET_INTENSITY);
            fflush(stdout);
        }


        // DO TARGET DETECTION
        Mat imgThresholded(imgOriginal.rows, imgOriginal.cols, CV_16UC1);
        processImages(imgDepth, imgOriginal, imgThresholded, GLOBAL_RETURNS);
    
        Mat trackedDepth;
        imgDepth.convertTo(trackedDepth, CV_16U);
        applyGain(imgOriginal, imgOriginal);
        applyGain(trackedDepth, trackedDepth);

        // OUPUT THE TARGET POSITIONS AND DRAW RECTANGLES FOR VISUALIZATION
        if (SHOW_TRACKING == 1) {
            for (int trkIdx=0; trkIdx<GLOBAL_RETURNS.size(); trkIdx++) {
                circle(trackedDepth, GLOBAL_RETURNS[trkIdx].depthLoc, 15, 0, 4);
                circle(trackedDepth, GLOBAL_RETURNS[trkIdx].depthLoc, 20, 65535, 4);
                rectangle(imgOriginal, GLOBAL_RETURNS[trkIdx].brect.tl(), GLOBAL_RETURNS[trkIdx].brect.br(), 65535, 2, CV_AA);
                rectangle(imgThresholded, GLOBAL_RETURNS[trkIdx].brect.tl(), GLOBAL_RETURNS[trkIdx].brect.br(), 65535, 2, CV_AA);
                rectangle(imgOriginal, GLOBAL_RETURNS[trkIdx].brect.tl(), GLOBAL_RETURNS[trkIdx].brect.br(), 0, 1, CV_AA);
                rectangle(imgThresholded, GLOBAL_RETURNS[trkIdx].brect.tl(), GLOBAL_RETURNS[trkIdx].brect.br(), 0, 1, CV_AA);
            }
        }

        // PUBLISH DEBUG IMAGES TO ROS TOPICS
        sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "mono8", imgThresholded).toImageMsg();
        irMaskedPub.publish(msg);
        msg = cv_bridge::CvImage(std_msgs::Header(), "mono16", imgOriginal).toImageMsg();
        irTrackedPub.publish(msg);
        msg = cv_bridge::CvImage(std_msgs::Header(), "mono16", trackedDepth).toImageMsg();
        depthTrackedPub.publish(msg);

        // OUPUT THE TARGET POSITIONS AND DRAW RECTANGLES FOR VISUALIZATION
        std_msgs::Float32MultiArray pos;
        pos.data.clear();
        for (int trkIdx=0; trkIdx<GLOBAL_RETURNS.size(); trkIdx++) { 
            pos.data.push_back(GLOBAL_RETURNS[trkIdx].xpos);
            pos.data.push_back(GLOBAL_RETURNS[trkIdx].ypos); 
            pos.data.push_back(GLOBAL_RETURNS[trkIdx].zpos);

            if (DEBUG_LEVEL > 0) {
                fprintf(stdout, "trackID = %d\txpos = %2.3f\typos = %2.3f\tzpos = %2.3f\taz = %2.3f\tel = %2.3f\trange = %2.3f\n", trkIdx, GLOBAL_RETURNS[trkIdx].xpos, GLOBAL_RETURNS[trkIdx].ypos, GLOBAL_RETURNS[trkIdx].zpos, GLOBAL_RETURNS[trkIdx].az, GLOBAL_RETURNS[trkIdx].el, GLOBAL_RETURNS[trkIdx].range);
            }
        }
        targetPosPub.publish(pos);
        ros::spinOnce();
    }
}

int main(int argc, char** argv)
{
    if (!CALIBRATION_COMPLETE && (PERFORM_CALIBRATION == 1)) {
        fprintf(stdout, "\n\n\nTime to calibrate the camera.\n"
                "Hold object of desired temperature\n"
                "in front of the camera so that it \n"
                "completely obscures the lens.\n\n");

        fprintf(stdout, "Calbration beginning in 3 seconds...\n");
        fflush(stdout);
        
        usleep(0.5*1000000);
        fprintf(stdout, "3...");
        fflush(stdout);

        usleep(0.5*1000000);
        fprintf(stdout, "2...");
        fflush(stdout);

        usleep(0.5*1000000);
        fprintf(stdout, "1...");
        fflush(stdout);

        usleep(0.5*1000000);
        fprintf(stdout, "\nCalibrating...\n");
        fflush(stdout);
    }

    // INITIATE PUBLISHERS
    ros::init(argc, argv, "flir_lepton_tracker_node");
    ros::NodeHandle nh;
    image_transport::ImageTransport it(nh);

    image_transport::Publisher irTrackedPub = it.advertise("flir_image_tracked", 1);
    image_transport::Publisher irMaskedPub = it.advertise("flir_image_masked", 1);
    image_transport::Publisher depthTrackedPub = it.advertise("depth_image_tracked", 1);

    ros::Publisher targetPosPub = nh.advertise<std_msgs::Float32MultiArray>("target_position", 1);

    waitKey(30);

    thread doTrackingThread(doTracking, irTrackedPub, irMaskedPub, depthTrackedPub, targetPosPub);

    // INITIATE IMAGE SUBSCRIBER
    image_transport::Subscriber flirSub = it.subscribe("flir_image_raw", 1, getFlirImage);
    image_transport::Subscriber depthSub = it.subscribe("depth_image_raw", 1, getDepthImage);

    ros::spin();

    
    return 0;
}
