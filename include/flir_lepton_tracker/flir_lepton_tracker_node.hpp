/*
 * trackerMain.h
 *
 *  Created on: Oct 6, 2015
 *      Author: taylordean
 */

#ifndef TRACKERMAIN_HPP_
#define TRACKERMAIN_HPP_

#include <image_transport/image_transport.h>
#include <flir_lepton_tracker/OBJECT_CONTAINER.hpp>
#include <cv_bridge/cv_bridge.h>


#define PI                              3.1415962153589

using namespace cv;
using namespace std;

void applyGain(const Mat imgIn, Mat& imgOut);
void getDepthImage(const sensor_msgs::ImageConstPtr& msg);
void getFlirImage(const sensor_msgs::ImageConstPtr& msg);
void morphImage(const Mat imgIn, Mat& imgOut, const int iLow, const int iHigh);
void getTargets(OBJECT_CONTAINER& objects, Mat imgContour, const Mat imgDepth, const Mat imgOriginal);
void findCentroid(const vector< Point > points, Point& centroid);
void processImages(const Mat imgDepth, const Mat imgOriginal, Mat& imgThresholded, OBJECT_CONTAINER& GLOBAL_RETURNS);
void calculateObjectParameters(OBJECT& target, const Mat imgDepth, const Mat imgOriginal);
bool isTargetGood(const OBJECT target);
void doTracking(image_transport::Publisher irTrackedPub, image_transport::Publisher irMaskedPub, image_transport::Publisher depthTrackedPub, ros::Publisher targetPosPub);
#endif /* TRACKERMAIN_HPP_ */
